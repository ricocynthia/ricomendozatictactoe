;; tictactoe.lisp
;;
;; Functions to help play a game of tic-tac-toe
;; Cynthia Rico Mendoza
;;

;; Just here as a handy board to test, in general don't use globals
(defparameter *brd* (list '- '- '- '- '- '- '- '- '-))   ;; also setf
(defvar *spaceNumber1*) ;; X move input
(defvar *spaceNumber2*) ;; O move input
(defvar wonGame 0)      ;; 0 = false
(defvar *gameType*)     ;; Variable to keep track of what type of game the user wants to play
(defvar *userCharacter*)  ;; Keep track of the user choice of being X or O -- when playing against AI
(defvar AIcharacter)    ;; Keep track of whether AI is O or X  
(defvar AIRandomNumber (+ 0 (random 8)))  ;; Creating random number to mark space on board


;;;;;;;;;;;;;;;;;;;; Steinmetz methods ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Prints a tic-tac-toe board in a pretty fashion.
;; Param: board - a list containing elements of a ttt board in row-major order
(defun print-board (board)
  (format t "=============")
  (do ((i 0 (+ i 1)) )
      ((= i 9) nil)
      (if (= (mod i 3) 0) 
          (format t "~%|")
          nil)
      (format t " ~A |" (nth i board))
  )
  (format t "~%=============")
)

;; Grabs the nth row of a tic-tac-toe board as a list
(defun grab-row (board row)
  (let ( (x (* 3 row)))
    (list (nth x board) (nth (+ 1 x) board) (nth (+ 2 x) board))
  )
)

;; Grabs the nth column of a tic-tac-toe board as a list
(defun grab-col (board col)
  (list (nth col board) (nth (+ 3 col) board) (nth (+ 6 col) board)))

;; Grabs nth crossed columns of a tic-tac-tow board as a list
(defun grab-cross-col (board col)
  (let ( (x (* 2 col)))
    (if (= x 0)
        (list (nth col board) (nth (+ 4 col) board) (nth (+ 8 col) board))
        (list (nth (+ 1 col) board) (nth (+ 3 col) board) (nth (+ 5 col) board)))
  )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;; Checking for all types of ways to win on tic-tac-toe board ;;;;;;;;;;;;;;;;;;

;; Checking for horizontal wins on borad
(defun check-horizontal-wins (board index)
  (setq xList (list 'X 'X 'X))
  (setq oList (list 'O 'O 'O))
  
  (do ( (row 0 (+ row 1)) )
      ( (= row 3) nil)
      
      ;; Check each row for any wins by comparing to oList and xList
      (if (or (equal (grab-row board row) xList ) (equal (grab-row board row) oList ) ) 
          (won-game index)
          nil)
  )
)

;; Checking for vertical wins on borad
(defun check-vertical-wins (board index)
  (setq xList (list 'X 'X 'X))
  (setq oList (list 'O 'O 'O))
  
  (do ( (col 0 (+ col 1)) )
      ( (= col 3) nil)
      
       ;; Check each column for any wins by comparing to oList and xList
      (if (or (equal (grab-col board col) xList ) (equal (grab-col board col) oList ) ) 
          (won-game index)
          nil)
  )
)

;; Checking for cross wins on borad
(defun check-cross-wins (board index)
  (setq xList (list 'X 'X 'X))
  (setq oList (list 'O 'O 'O))
  
  (do ( (colNum 0 (+ colNum 1)) )
      ( (= colNum 2) nil)
      
       ;; Check each cross column for any wins by comparing to oList and xList
      (if (or (equal (grab-cross-col board colNum) xList ) (equal (grab-cross-col board colNum) oList ) ) 
          (won-game index)
          nil)
  )
)

;; Notifies the users when player has won
(defun won-game (index)

  ;; set wonGame to 1 (1=true)
  (setq wonGame 1)

  ;; Figure out if player is x or o
    (if (= (mod index 2) 0)
      (setq player 'X) (setq player 'O))
  
  ;; Congratulate user
  (terpri)  ;; print new line
  (format t "Congratulations player ~A, you won! " player)
  (terpri)  ;; print new line
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;; Setting spot on tic-tac-toe board ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Removes the extra space created when reading/adding the user's input space number
(defun remove-nth (n list)
  (declare
    (type (integer 0) n)
    (type list list))
  (if (or (zerop n) (null list))
    (cdr list)
    (cons (car list) (remove-nth (1- n) (cdr list)))))



;; Prompting the users to begin
;; x will always be first
(defun mark-spaceNumber (board spaceNumber val index)
    (defvar tempBoard)
    (setq tempBoard '())
    (do ((i 0 (+ i 1)) )
      ((= i 9) nil)
      (if (=  i spaceNumber) 
          (setq tempBoard (append tempBoard (list val)))
          nil)
        (setq tempBoard (append tempBoard (list (nth i board))))
    )
    (setq tempBoard (remove-nth (+ spaceNumber 1) tempBoard))
    (setq *brd* tempBoard)

    ;; After making a mark on the board, this is a good time to 
    ;; check if there are any wins on the board
    (check-cross-wins *brd* index)
    (check-horizontal-wins *brd* index)
    (check-vertical-wins *brd* index)

)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;; Functions to help check if move is legal ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
;; Prompts the user to re-enter their move if they chose an illegal move
(defun reenter-move (spaceNumber board index )
  (print "invalid move. please try again.")
  (setq spaceNumber (read))
  (check-move spaceNumber board index ) ;; Check to see if the new user input value is legal on board
)

;; Checks to see if the inputed space number already has an X or O
(defun check-move (spaceNumber board index )

    ;; Figure out if player is x or o
    (if (= (mod index 2) 0)
      (setq player 'X) (setq player 'O))

    (setq spaceNumValue (nth spaceNumber board))  ;; Getting the value of the spaceNumber index of board  

    ;; Checking for illegal moves
    (if (or (or (> spaceNumber 8) (< spaceNumber 0)) (or (string= spaceNumValue #\X) (string= spaceNumValue #\O)))
        (reenter-move spaceNumber board index ) ;; invalid move -- have user try again.
        (mark-spaceNumber board spaceNumber player index))    ;; mark spot on board once we know the space number is valid

) 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;; Two player functions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Prompts player X to put move on board
(defun promptX (i)
    (print "Player X: Please indicate where you want to place your mark on the board...")
    (setq *spaceNumber1* (read))  ;; Reading player X desired spot destination
    (check-move *spaceNumber1* *brd* i)
)

;; Prompts player O to put move on board
(defun promptO (i)
    (print "Player O: Please indicate where you want to place your mark on the board...")
    (setq *spaceNumber2* (read))  ;; Reading player O desired spot destination
    (check-move *spaceNumber2* *brd* i)
)

;;;;;;;;; Prompt user to choose what kind of game they want to play ;;;;;;;;;;;;;;;;;;;;;;;

(defun choose-game-version ()
  (print "Please indicate how you would like to play tic-tac-toe.")
  (print "0 - AI vs AI")
  (print "1 - AI vs User")
  (print "2 - User vs User")
  (print "Press the number 0, 1, or 2 to start a game.")
  (setq *gameType* (read))

  ;; Calling the correct version of the tic-tac-toe game
  (if (= *gameType* 0)
      (play-game-v0 *brd*) nil)
  (if (= *gameType* 1)
      (play-game-v1 *brd*) nil)
  (if (= *gameType* 2)
      (play-game-v2 *brd*) nil)
  
)

;; Allows the user to choose if they want to be X or O
(defun user-choose-character () 
  (print "Since you chose to play against the computer, would you like to be X or O? ")
  (setq *userCharacter* (read))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;; AI functions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun AI-move (index AICharValue) 

  (setq spaceNumValue (nth AIRandomNumber *brd*))

  ;; Checking for illegal moves
  (if (or (string= spaceNumValue #\X) (string= spaceNumValue #\O))
      (regenerate-number index AICharValue) ;; invalid move -- have user try again.
      (mark-spaceNumber *brd* AIRandomNumber AICharValue index))    ;; mark spot on board once we know the space number is valid
)

;; Setting AI character as X or O based on user's choice
(defun set-AI-character-v1 (userCharacter)
  (if (string= userCharacter 'X)
      (setq AIcharacter 'O)
      (setq AIcharacter 'X))
)

(defun regenerate-number (index AICharValue)
  (setq AIRandomNumber (+ 0 (random 8)))
  (AI-move AIRandomNumber AICharValue)
)



(defun mark-X (i) 
  ;; Checking whether the user or AI goes first (x always first)
  (if (string= *userCharacter* 'X)
      (promptX i ) ;; prompt the user to place an X on the board
      (AI-move i 'X)  ;; if userCharacter = O, AI will be X and begin the game
  )
)

(defun mark-O (i) 
  ;; Checking whether the user or AI goes first (x always first)
  (if (string= *userCharacter* 'O)
      (promptO i ) ;; prompt the user to place an O on the board
      (AI-move i 'O)  ;; if userCharacter = X, AI will be O
  )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;; Functions for each version of the game ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; AI vs AI
(defun play-game-v0 (board)

  (do ((i 0 (+ i 1)) )
      ((= i 9) (print "Bummer. Looks like a tie. Game over."))
        (if (= (mod i 2) 0) 
            (AI-move i 'X)
            (AI-move i 'O)
        )

        (print-board *brd*)

        ;; Checking wonGame value is true
        ;; If true (wonGame = 1), exit the loop/game
        (if (= wonGame 1)
            (return ) nil)
  )
)

;; AI vs User
(defun play-game-v1 (board)
  (user-choose-character)   ;; Allowing the user to choose if they want to be X or O
  (set-AI-character-v1 *userCharacter*)   ;; Setting AI character to X or O based on user's choice

  (print "Indicate where you want to place your move by typing 0 - 8. 0 being the top left, 8 being the bottom right spot.")
    (do ((i 0 (+ i 1)) )
      ((= i 9) (print "Bummer. Looks like a tie. Game over."))
        (if (= (mod i 2) 0) 
            (mark-X i)
            (mark-O i)
        )

        (print-board *brd*)

        ;; Checking wonGame value is true
        ;; If true (wonGame = 1), exit the loop/game
        (if (= wonGame 1)
            (return ) nil)
  )

)

;; User vs. User
(defun play-game-v2 (board)
    (print "Indicate where you want to place your move by typing 0 - 8. 0 being the top left, 8 being the bottom right spot.")
    (do ((i 0 (+ i 1)) )
      ((= i 9) (print "Bummer. Looks like a tie. Game over."))
        (if (= (mod i 2) 0) 
            (promptX i ) ;; need to pass i to check move
            (promptO i ))

        (print-board *brd*)

        ;; Checking wonGame value is true
        ;; If true (wonGame = 1), exit the loop/game
        (if (= wonGame 1)
            (return ) nil)
  )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;; Call main function ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(choose-game-version) ;; Prompt the user to select game version

