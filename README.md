This project will allow a user to play a tic-tac-toe game through the command line.
This program is made using LISP.
Note: In order to run this program you will need to download CLISP.

Game Versions:
    1) 0 - AI vs AI (0 players)
    2) 1 - AI vs User (1 player)
    3) 2 - User vs User (2 player)

These are the current functionalities of the program:
    1) Print the tic-tac-toe board
    2) Allowing the player to put a move on the board
    3) Checking whether the move is legal or not
    4) Checks to see if three items in a row/col/cross col are equal to each other
        in order to detect a winner of the game
    5) Grabs a row, col, and cross columns of the board
    6) Prompts the users to put a move on the board

How to run the program:
    1) Download the repo
    2) Open cmd line
    3) Cd to the location of the repo
    4) Run the following on the cmd line: clisp tictactoe.lisp
    5) Enjoy!
    
    Note: To play again after a player has won, re-type 'clisp tictactoe.lisp' on the cmd line. (Make sure you are in the repo directory)

How to choose game version:
    When game is first ran, user will be prompted to choose the type of game they want to play.
    User may choose from the following game options:
        0 players: AI vs AI
        1 player: AI vs User
        2 players: User vs User
    User selects the game type by inputting the corresponding number of the game followed by clicking the ENTER key.

How to play Version 0 (AI vs AI):
    After selecting game type, the game will display a board for each move and then print the winning board and congratulate the player that won.

How to play Version 1 (AI vs User):
    After selecting game type, user is prompted to input their desired tic-tac-toe character.
    User should simply type X or O followed by the ENTER key. Then, to put a move on the board type in numbers 0 - 8 followed by the ENTER key, when you are prompted to add a move.

How to play Version 2 (User vs User):
    After selection game type, player X is prompted to start the game. It is amoung the two players to decide who is X and O -- player X always starts first. To put a move on the board type in numbers 0 - 8 followed by the ENTER key, when you are prompted to add a move. Then, player O will be prompted to add a move. Player X and O alternate turns -- screen will indicate when it is X or O turn. 

How to play again:
    In order to play again, you will need to run through the "How to run the program" section of this file. Once you run "clisp tictactoe.lisp" again, you are able to play again.

How to stop the game:
    If you want to stop the game in the middle of it, please press CTRL C. This will abort the game and let you restart/play again.


Cynthia Rico Mendoza